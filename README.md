# Project Red-Generation | Author: JustRed | since 2014

# File purpose

This file is just a readme-file in which we want to announce our players what they should do when they find a bug or other error to our script.
If you found or if you find a bug here is the right place to report.

Direct link: https://bitbucket.org/justred/red-generation/

# BIG UPDATE - 15.04.2017 until X


- Sistemul de Loterie a fost schimbat in totalitate. Loteria se porneste la ora 20:00
sau prin comanda /startlotto.
- Exista 6 Numere de la 1 pana la 11 (sansele de castig sunt mai mari acum). 
- Cand ai 3 numere valabile o sa castigi 25% din Jackpot, la 4 numere valabile o sa castigi 50% din Jackpot, 
la 5 numere 75% din Jackpot si la 6 Numere 100% din Jackpot.
- Ordinea numerelor nu conteaza acum! Minimul de castig este de 3 numere valabile.
- Comanda /fly a fost restrictionata in asa fel astfel sa nu se poata abuza de ea. 
- Au fost adaugate loguri pentru comenzile in care un jucator primeste bani si altuia i se ia. Toate logurile se pot verifica ulterior pe Panel.
- Exemplu log PayDay: +594$ | Sender: PayDay sau -343$ | Receiver: JustRed.
- Paylogs sunt pentru a va ajuta sa tineti evidenta banilor acumulati/consumati ai contului dvs. La sfarsitul tabelei se face un sumar saptamanal de cati bani au intrat si cati au iesit. 
- Comanda /charity a fost actualizata - memory lack fix / doar cu PIN-ul puteti sa o folositi acum.
- Comanda /givecharity a fost actualizata - memory lack fix / doar cu PIN-ul puteti sa o folositi acum.
- Comanda /healto a fost actualizata - cod intern, memory lack fix.
- Comanda /call a fost actualizata - memory lack fix / doar cu PIN-ul puteti sa o folositi acum. 
- Comanda /sellcartostate este acum restrictionata de PIN-ul dvs.
- Rezolvat un bug de la /transfer - o sa ia acum taxa exacta afisata in dialog.
- Paylogs au fost create pentru cele mai importante comenzi si functii (unde se schimba sume mari de bani).  
- Valorile comenzilor /giveallmoney sau /giveallrp au fost scazute si necesita acum cod PIN.
- Premiul pentru Achievementul #1: Join a faction - a fost marit cu 150%.
- Adaugata comanda /giveallcoins pentru a da jucatorilor online Silver Coins.
- Rezolvat bugul de la /stopanim - nu se mai poate folosi in vehicule.
- Rezolvat un bug de scriere de la /bail. 
- Rezolvat un bug de la IESIREA din casa in modul Sleep.
- Rezolvat un bug de la /tazer - nu o sa mai dea FULL HP dupa ce tazerul expira.
- Rezolvat un bug de la Fight Arena, Paint Arena & Racing Arena - chaturile nu se mai pot folosi cand aveti restrictie de mute. 

# Factions

- Au fost adaugate 2 masini de tip Mesa si 2 masini de tip Turismo la factiunea National Guard. 
- Cele 2 masini de tip Mesa se pot conduce de la Rank 3+ de orice politist.
- Cele 2 masini de tip Turismo se pot conduce de la Rank 4+ de orice politist. 

# Jobs

- Comanda /deliver nu se mai poate folosi daca nu aveti licenta de condus. 
- La jobul Curier nu se mai face spam cand trebuie sa livrati pachetul. 
- Sectiunea /tutorial - Slujbe a fost editata.
- Jobul Detective a fost actualizat.
- Castigurile la jobul Detectiv de asemenea au fost marite.
- Jobul de Detectiv a devenit acum cel mai profitabil job per tura, insa, exista timp de pauza intre work-time, in functie de skill care-l egalizeaza cu celalalte joburi.
- Rezolvat un bug la jobul Fisher.
- Statistica joburilor a fost refacuta cu un aspect mai placut si inca o optiune.
- Pickup-urile de la punctul de angajare de la joburi au fost schimbate.
- Comanda /work de la Car Jacker a fost inlocuita cu comanda /stealcar.

# War System updates

- Secundele pe turf se salveaza acum dupa relog. 

# Easter Quest

- A fost adaugat un nou Easter Quest.
- Trebuie sa gasiti 20 de oua situate in orasul Los Santos.
- Premiul este: 10 Silver Coins, 30.000$ si 10 Respect Points. 

# New ideas

Also, you can also help us to develop the best gamemode by just writting us a suggestion that you think will really helpful for players 
and for server. 

# Server IP

rpg.red-generation.com

# Forum

http://red-generation.com/forum/

# Panel

http://red-generation.com/panel/